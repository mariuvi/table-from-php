<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .product{
            width: 25vw;
            display: inline-block;
            margin: 10px;
            padding: 30px;
            background-color: beige;
        }
    </style>
</head>
<body>

<?php



$products = [
    [
        'title' => 'Product 1',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 2',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 3',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 4',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 5',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 6',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],
    [
        'title' => 'Product 7',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, nulla officia quas rem tempore veniam?elit. Architecto, nulla officia quas rem tempore veniam?',
        'price' => 4.34,
    ],

];

$i = 0;

foreach ($products as $product) {

        echo '<div class="product">';
        echo '<h2>'.$product['title'].'</h2>';
        echo '<p>'.substr($product['description'], 0, 200).'</p>';
        echo '<h4>'.$product['price'].'$</h4>';
        echo '</div>';
        $i++;

    if($i == 3){
        echo '<hr>';
        $i = 0;
    }
}

?>
</body>
</html>

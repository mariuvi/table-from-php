<?php

declare(strict_types  = 1);

$menu = [
    [
        'title' => 'Home',
        'link' => '/',
    ],
    [
        'title' => 'Products',
        'link' => 'products',
        'submenu' => [
            [
                'title' => 'Naujausi',
                'link' => 'newest',
            ],
            [
                'title' => 'Perkamiausi',
                'link' => 'best-selling',
            ],
            [
                'title' => 'Ispardavimas',
                'link' => 'sales',
            ],

        ],
    ],
    [
        'title' => 'About us',
        'link' => 'about-as',
    ],
    [
        'title' => 'Contacts',
        'link' => 'contacts',
    ],
    [
        'title' => 'Data',
        'link' => 'data.php',
    ],
];

echo '<ul>';
foreach ($menu as $item) {

    echo '<li><a href="'.$item['link'].'">'.$item['title'].'</a></li>';
    if (isset($item['submenu']) && !empty($item['submenu'])) {
        echo '<ul>';
        foreach ($item['submenu'] as $submenu) {
            echo '<li><a href="'.$submenu['link'].'">'.$submenu['title'].'</a></li>';
        }
        echo '</ul>';
    }
}
echo '</ul>';
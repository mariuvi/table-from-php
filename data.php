<?php
declare(strict_types  = 1);

$data = [
    [
        'brand' => 'Audi',
        'model' => 'A3',
        'year' => '1997',
        'color' => 'blue',
    ],
    [
        'brand' => 'Audi',
        'model' => 'A6',
        'year' => '2000',
        'color' => 'red',
    ],
    [
        'brand' => 'BMW',
        'model' => '730',
        'year' => '2010',
        'color' => 'yellow',
    ],
//    [
//        'brand' => 'Citroen',
//        'model' => 'Picaso',
//        'year' => '2001',
//        'color' => 'black',
//    ],
];

echo '<table border="1">';

echo '<tr>';

echo '<th>Brand</th>';
echo '<th>Model</th>';
echo '<th>Year</th>';
echo '<th>Color</th>';

echo '</tr>';
$i = 0;

foreach ($data as $index =>$auto){
//    if ($i >= 3){
//        break;
//    }
    echo '<tr>';
    echo '<td>'.$auto['brand'].'</td>';
    echo '<td>'.$auto['model'].'</td>';
    echo '<td>'.$auto['year'].'</td>';
    echo '<td>'.$auto['color'].'</td>';
    echo '</tr>';
    $i++;
}

echo '</table>';

echo '</br>';




echo '<table border="1">';

echo '<tr>';

echo '<th>Auto in row</th>';

echo '</tr>';



$i = 0;
$j = 0;
$count = count($data);

foreach ($data as $auto){
if ($i == 0){

    echo '<tr>';

    echo '<td>';
}
    echo $auto['brand'];
    $i++;
    $j++;

    if ($i == 2 || $j >= $count){

    echo '</td>';
    echo '</tr>';
    $i = 0;
    }


}

echo '</table>';